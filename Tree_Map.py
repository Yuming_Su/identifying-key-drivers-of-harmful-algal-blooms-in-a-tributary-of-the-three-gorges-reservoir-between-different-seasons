
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.ensemble import ExtraTreesRegressor
from Data_Standard import data_prepard,data_labels,data_attribs
import pandas as pd
import squarify

Data_Path="Datasets/"


ETS=ExtraTreesRegressor(n_estimators=,max_depth=,random_state=)

ETS.fit(data_prepard,data_labels)

Feature_Importance=ETS.feature_importances_


feature_importance=ETS.feature_importances_
feature_importance_dict={'features':data_attribs,'feature importance':feature_importance}
feature_importance_pd=pd.DataFrame(feature_importance_dict)
feature_importance_pd.sort_values('feature importance',inplace=True,ascending=False)

print(feature_importance_pd)



features=feature_importance_pd['features'].values.tolist()

importance=feature_importance_pd['feature importance'].values.tolist()

sum_importance=sum(importance)

feature_importance=[]
feature_importance_=[]

for i in importance:
    round(i,2)
    a=100*(i/sum_importance)
    b = str(round(a, 1)) + '%'
    feature_importance.append(round(a, 1))
    feature_importance_.append(b)

feature_importance_pd['feature importance']=feature_importance_


params = {
          'axes.titlesize': 30,
          'legend.fontsize': 26,
          'font.family':'Times New Roman',
          'font.weight': "bold",
          "axes.labelweight": "bold",
          "figure.titleweight": "bold",
          "axes.labelsize":28,
          "xtick.labelsize":26,
          "ytick.labelsize":26,
          "savefig.dpi":300,
          'figure.dpi':300,

          }

plt.rcParams.update(params)
#plt.style.use('seaborn-whitegrid')
#sns.set_style("white")

plt.rcParams['axes.unicode_minus'] = False



labels = feature_importance_pd.apply(lambda x: str(x[0]) + "\n  (" + str(x[1]) + ")", axis=1)
sizes = feature_importance
#colors = [plt.cm.Spectral( i /float(len(labels))) for i in range(len(labels))]

colors =['#f94144','#f3722c','#f9844a','#f9c74f', '#90be6d', '#43aa8b','#4d908e'
    ,'#577590','#277da1'] #Full

#colors =['#d9ed92','#b5e48c','#76c893', '#52b69a','#34a0a4','#168aad'
    #,'#1a759f','#1e6091','#184e77'] #Wet

#colors =['#ef233c','#EF4136','#f05b72','#f38375'
    #,'#ffb5a7','#fcbf49','#ffd000','#ffea00','#efea5a'] #Dry







plt.figure(figsize=(24 ,10), dpi= 300)

squarify.plot(sizes=sizes, label=labels, color=colors, alpha=.8,text_kwargs={'fontsize':30})

    # Decorate
plt.title('Full   Period', fontdict={'size': 36})

plt.axis('off') 
#plt.title(title)
plt.show()
