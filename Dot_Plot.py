from Data_Loading import Data
import matplotlib.pyplot as plt


df=Data
df.sort_values('MIC',inplace=True)
df.reset_index(inplace=True)


rcParams={'font.family':'Times New Roman',
          'font.weight': "bold",
          "axes.labelweight": "bold",
          "figure.titleweight": "bold",
          "axes.labelsize":28,
          "xtick.labelsize":26,
          "ytick.labelsize":26,
          "savefig.dpi":300,
          'figure.dpi':300,}

plt.rcParams.update(rcParams)

fig, ax = plt.subplots(figsize=(8,15), dpi= 300)
ax.hlines(y=df.index, xmin=0.05, xmax=0.65, color='gray', alpha=0.9, linewidth=2, linestyles='dashdot')
ax.scatter(y=df.index, x=df.MIC, s=220, color='blue', alpha=0.9)

#saddlebrown
ax.set_title('Wet Season',fontdict={'size':32})
ax.set_xlabel('MIC')
#ax.set_ylabel('Factors')
ax.set_yticks(df.index)
ax.set_yticklabels(df.Factors, fontdict={'horizontalalignment': 'right'})
ax.set_xlim(0, 0.7)
plt.show()