import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from Correlation_MIC import MIC_Mat_continue_pd

plt.rc('font', family='Times New Roman')
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelweight"] = "bold"


def ShowHeatMap(DataFrame):
    ylabels = DataFrame.columns.values.tolist()
    f, ax = plt.subplots(figsize=(16, 15))
    ax.set_title('Wet Season', fontdict={'size':34})
    mask = np.zeros_like(DataFrame, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True
    sns_plot=sns.heatmap(DataFrame.astype(float),
                mask=mask,
                linewidths=0.3,
                cmap='OrRd',
                ax=ax,
                fmt='.2g',
                annot=False,
                         annot_kws={'size': 16},
                yticklabels=ylabels,
                xticklabels=ylabels,
                vmax=1,
                         vmin=0)
    sns_plot.tick_params(labelsize=22)
    cax = plt.gcf().axes[-1]
    cax.tick_params(labelsize=26)


    plt.show()

ShowHeatMap(MIC_Mat_continue_pd)

