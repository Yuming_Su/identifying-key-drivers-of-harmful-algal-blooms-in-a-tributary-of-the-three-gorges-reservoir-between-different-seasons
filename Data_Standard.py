from Data_Loading import Data
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator,TransformerMixin
from sklearn.utils import shuffle

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names].values

Data_ = shuffle(Data,random_state=29)

data_labels=Data_['Chla']

data=Data_.drop("Chla", axis=1)

data_attribs=list(data)


data_pipeline=Pipeline([('selector',DataFrameSelector(data_attribs)),('std_scalar',StandardScaler())])


data_prepard=data_pipeline.fit_transform(data)

