import seaborn as sns
from Data_Loading import load_data
import matplotlib.pyplot as plt

Data_Path="Datasets/"
data=load_data(File_Path='grouped bar.xlsx') #the file 'grouped bar' created by MIC value


rcParams={'font.weight': "bold",
          "axes.labelweight": "bold",
          "figure.titleweight": "bold",
          "axes.labelsize":30,
          "xtick.labelsize":24,
          "ytick.labelsize":24,
          "legend.fontsize":24,
           "savefig.dpi":300,
          'figure.dpi':300,}





sns.set(style="whitegrid", font="Times New Roman",rc=rcParams)
f, ax = plt.subplots(figsize=(28, 8))
ax.set_title('', fontdict={'size': 34})

sns.barplot(y='MIC',x='Factors',hue='Lag Months',data=data,palette=(sns.color_palette("", 4)))


plt.show()





