import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.neural_network import MLPRegressor
from Data_Standard import data_prepard,data_labels,data_attribs
import pandas as pd



Data_Path="Datasets/"


MLP=MLPRegressor(hidden_layer_sizes=(),random_state=29)




def model_score(model,x,y):
    rmse=cross_val_score(model, x, y,scoring="neg_mean_squared_error", cv=10)
    rmse_score= np.sqrt(-rmse)
    rmse_mean=rmse_score.mean()
    rmse_std=rmse_score.std(ddof=1)
    r2=cross_val_score(model, x ,y ,scoring='r2', cv=10)
    r2_mean=r2.mean()
    r2_std=r2.std(ddof=1)#无偏估计
    model_scores=[rmse_score,rmse_mean,rmse_std,r2,r2_mean,r2_std]
    return model_scores




MLP_score=model_score(MLP,data_prepard,data_labels)




writer = pd.ExcelWriter('MLP_score.xlsx')

model_scores =MLP_score
rmse_score = pd.DataFrame(model_scores[0])
rmse_mean = model_scores[1]
rmse_std = model_scores[2]
r2 = pd.DataFrame(model_scores[3])
r2_mean = model_scores[4]
r2_std = model_scores[5]
result = [rmse_mean, rmse_std, r2_mean, r2_std]
result_name = pd.DataFrame(['rmse_mean', 'rmse_std', 'r2_mean', 'r2_std'])
result_pd = pd.DataFrame(np.array(result))

model_scores_pd = pd.concat([rmse_score, r2, result_pd, result_name], axis=1)
model_scores_pd.columns = ['RMSE', 'R2', 'Results', 'Results_name']

model_scores_pd.to_excel(writer, sheet_name='MLP')
writer.save()




