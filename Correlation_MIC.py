from minepy import MINE
from Data_Loading import Data
import pandas as pd
import numpy as np



def MIC_matirx(dataframe, mine):

    data = np.array(dataframe)
    n = len(data[0, :])
    result = np.zeros([n, n])

    for i in range(n):
        for j in range(n):
            mine.compute_score(data[:, i], data[:, j])
            result[i, j] = mine.mic()
            result[j, i] = mine.mic()
    RT = pd.DataFrame(result)
    return RT

mine=MINE(0.6,15)

MIC_Mat_continue_pd=MIC_matirx(Data,mine)

MIC_matirx_continue_attri=list(Data)

MIC_Mat_continue_pd.columns=MIC_matirx_continue_attri
MIC_Mat_continue_pd.index=MIC_matirx_continue_attri

writer = pd.ExcelWriter('MIC_mat_continue.xlsx')
MIC_Mat_continue_pd.to_excel(writer, sheet_name='sheet1', index=True)

writer.save()




